<?php

namespace Drupal\migrate_process_markdown_to_html\Plugin\migrate\process;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;
use League\CommonMark\Environment\Environment;
use League\CommonMark\Extension\Attributes\AttributesExtension;
use League\CommonMark\Extension\Autolink\AutolinkExtension;
use League\CommonMark\Extension\CommonMark\CommonMarkCoreExtension;
use League\CommonMark\Extension\DescriptionList\DescriptionListExtension;
use League\CommonMark\Extension\DisallowedRawHtml\DisallowedRawHtmlExtension;
use League\CommonMark\Extension\Embed\EmbedExtension;
use League\CommonMark\Extension\ExtensionInterface;
use League\CommonMark\Extension\ExternalLink\ExternalLinkExtension;
use League\CommonMark\Extension\Footnote\FootnoteExtension;
use League\CommonMark\Extension\GithubFlavoredMarkdownExtension;
use League\CommonMark\Extension\HeadingPermalink\HeadingPermalinkExtension;
use League\CommonMark\Extension\InlinesOnly\InlinesOnlyExtension;
use League\CommonMark\Extension\Mention\MentionExtension;
use League\CommonMark\Extension\SmartPunct\SmartPunctExtension;
use League\CommonMark\Extension\Strikethrough\StrikethroughExtension;
use League\CommonMark\Extension\Table\TableExtension;
use League\CommonMark\Extension\TableOfContents\TableOfContentsExtension;
use League\CommonMark\Extension\TaskList\TaskListExtension;
use League\CommonMark\MarkdownConverter;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a markdown_to_html plugin.
 *
 * Usage:
 *
 * @code
 * process:
 *   bar:
 *     plugin: markdown_to_html
 *     source: foo
 *     markdown_extensions:
 *       - attributes
 *       - disallow_raw_html
 *       - embed
 *       - external_link
 *       - footnote
 *       - heading_permalink
 *       - inlines_only
 *       - mention
 *       - smart_punct
 *       - strikethrough
 *       - table
 *       - table_of_contents
 *
 * @endcode
 *
 * Note only the listed markdown extensions are supported.
 * They map to the implementations of ConfigurableExtensionInterface provided
 * by league/commonmark.
 *
 * @MigrateProcessPlugin(
 *   id = "markdown_to_html"
 * )
 */
class MarkdownToHtml extends ProcessPluginBase implements ContainerFactoryPluginInterface {

  /**
   * Converter for converting markdown to HTML.
   */
  protected MarkdownConverter $converter;

  /**
   * Constructs a MarkdownToHtml plugin.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The plugin ID.
   * @param mixed $plugin_definition
   *   The plugin definition.
   *
   * @throws \InvalidArgumentException
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $environment = new Environment([]);

    if (array_key_exists('markdown_extensions', $configuration) && is_array($configuration['markdown_extensions'])) {
      foreach ($configuration['markdown_extensions'] as $extension) {
        $environment->addExtension($this->getExtensionClass($extension));
      }
    }

    // Always add the CommonMarkCoreExtension.
    $environment->addExtension(new CommonMarkCoreExtension());
    $this->converter = new MarkdownConverter($environment);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): MarkdownToHtml {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
    );
  }

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    return $this->converter->convert($value)->getContent();
  }

  /**
   * Returns an instance of the extension class for the plugin config string.
   *
   * @param string $extension
   *   String from the markdown_extensions configuration for the process plugin.
   *
   * @return \League\CommonMark\Extension\ExtensionInterface
   *   Matching extension.
   *
   * @throws \InvalidArgumentException
   */
  protected function getExtensionClass(string $extension) : ExtensionInterface {
    return match ($extension) {
      'attributes' => new AttributesExtension(),
      'autolink' => new AutolinkExtension(),
      'description_list' => new DescriptionListExtension(),
      'disallow_raw_html' => new DisallowedRawHtmlExtension(),
      'embed' => new EmbedExtension(),
      'external_link' => new ExternalLinkExtension(),
      'footnote' => new FootnoteExtension(),
      'github_flavored_markdown' => new GithubFlavoredMarkdownExtension(),
      'heading_permalink' => new HeadingPermalinkExtension(),
      'inlines_only' => new InlinesOnlyExtension(),
      'mention' => new MentionExtension(),
      'smart_punct' => new SmartPunctExtension(),
      'strikethrough' => new StrikethroughExtension(),
      'table' => new TableExtension(),
      'table_of_contents' => new TableOfContentsExtension(),
      'task_list' => new TaskListExtension(),
      default => throw new \InvalidArgumentException(),
    };
  }

}
