# Migrate Process Markdown to HTML

## Table of contents

- Introduction
- Requirements
- Recommended modules
- Installation
- Configuration
- Maintainers

## Introduction

This module contains a simple [migrate process plugin](https://www.drupal.org/docs/8/api/migrate-api/migrate-process-plugins) for converting Markdown to HTML in your Drupal Migrations.

This module uses the [CommonMark](https://commonmark.thephpleague.com/) package from [The League of Extraordinary Packages](https://thephpleague.com/) to perform the transformation.

For a full description of the module, visit the [project page](https://www.drupal.org/project/migrate_process_markdown_to_html).

Submit bug reports and feature suggestions, or track changes in the [issue queue](https://www.drupal.org/project/issues/migrate_process_markdown_to_html).

## Requirements

This module requires no modules outside of Drupal core.
The core migrate module is required to include this process plugin in a data migration.

## Recommended modules

None.

## Installation

Install as you would normally install a contributed Drupal module. For further information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

1. Enable the module at Administration > Extend or via `drush pm:enable migrate_process_markdown_to_html`.
2. Include the process plugin in your pipeline configuration as follows:
```
process:
  bar:
    plugin: markdown_to_html
    source: foo
    markdown_extensions:
      - attributes
      - autolink
      - description_list
      - disallow_raw_html
      - embed
      - external_link
      - footnote
      - github_flavored_markdown
      - heading_permalink
      - inlines_only
      - mention
      - smart_punct
      - strikethrough
      - table
      - table_of_contents
      - task_list
```

Note the markdown_extensions are optional, and correspond to the [extensions](https://commonmark.thephpleague.com/2.3/extensions/overview/) supplied with the CommonMark package.

Support for more extensions may be added at a later date.

## Maintainers

Current maintainers:
- Elliot Ward (eli-t) - https://www.drupal.org/u/eli-t
