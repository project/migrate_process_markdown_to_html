<?php

declare(strict_types=1);

namespace Drupal\Tests\migrate_process_markdown_to_html\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\migrate\MigrateExecutable;
use Drupal\migrate\Plugin\MigrationInterface;

/**
 * Kernel test to actually run a migration.
 *
 * @group migrate_process_markdown_to_html
 */
class MigrationTest extends KernelTestBase {
  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'migrate',
    'migrate_process_markdown_to_html',
    'user',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installSchema('user', ['users_data']);
    $this->installEntitySchema('user');
  }

  /**
   * Runs a test migration passing a field through the Markdown plugin.
   *
   * This is a slightly contrived example that runs a migration that creates
   * two users, converting their names from Markdown to HTML. This test only
   * checks the migration completes successfully. Coverage could be extended
   * to check the saved entities, but the actual data transformations are well
   * covered by unit tests.
   */
  public function testMigrationExecutable(): void {

    $data_rows = [
      ['key' => '1', 'field1' => "# Alatar\n"],
      ['key' => '2', 'field1' => "# Pallando\n"],
    ];
    $ids = ['key' => ['type' => 'integer']];
    $definition = [
      'source' => [
        'plugin' => 'embedded_data',
        'data_rows' => $data_rows,
        'ids' => $ids,
      ],
      'process' => [
        'name' => [
          'plugin' => 'markdown_to_html',
          'source' => 'field1',
        ],
      ],
      'destination' => [
        'plugin' => 'entity:user',
      ],
    ];

    $migration = \Drupal::service('plugin.manager.migration')->createStubMigration($definition);
    $migrate_executable = new MigrateExecutable($migration);
    $this->assertEquals(MigrationInterface::RESULT_COMPLETED, $migrate_executable->import());
  }

}
