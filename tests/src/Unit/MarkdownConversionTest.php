<?php

declare(strict_types=1);

namespace Drupal\Tests\migrate_process_markdown_to_html\Unit;

use Drupal\migrate\MigrateExecutable;
use Drupal\migrate\Row;
use Drupal\migrate_process_markdown_to_html\Plugin\migrate\process\MarkdownToHtml;
use Drupal\Tests\UnitTestCase;
use PHPUnit\Framework\MockObject\MockObject;

/**
 * Class for testing markdown conversion.
 *
 * @coversDefaultClass \Drupal\migrate_process_markdown_to_html\Plugin\migrate\process\MarkdownToHtml
 *
 * @group migrate_process_markdown_to_html
 */
class MarkdownConversionTest extends UnitTestCase {

  /**
   * Mock of the MigrateExecutable.
   *
   * @var \Drupal\migrate\MigrateExecutable|\PHPUnit\Framework\MockObject\MockObject
   */
  protected MigrateExecutable|MockObject $migrateExecutable;

  /**
   * Mock of the Row.
   *
   * @var \Drupal\migrate\Row|\PHPUnit\Framework\MockObject\MockObject
   */
  protected Row|MockObject $row;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->migrateExecutable = $this->createMock('\Drupal\migrate\MigrateExecutable');
    $this->row = $this->createMock('\Drupal\migrate\Row');
  }

  /**
   * Test transformation of simple string with no special markdown.
   *
   * It should be transformed into the same text, wrapped in a <p> element and
   * followed by a newline.
   *
   * @covers ::transform
   */
  public function testPlainText(): void {
    $process_plugin = new MarkdownToHtml([], '', []);
    $markdown = 'I don’t know half of you half as well as I should like; and I like less than half of you half as well as you deserve.';
    $html = $process_plugin->transform(
      $markdown,
      $this->migrateExecutable,
      $this->row,
      ''
    );
    self::assertEquals("<p>$markdown</p>\n", $html);
  }

  /**
   * Tests various markdown elements are represented in transformed HTML.
   *
   * @covers ::transform
   */
  public function testElements(): void {
    $process_plugin = new MarkdownToHtml([], '', []);
    $test_data = [
      [
        'markdown' => '# A Long Awaited Party',
        'search_elements' => ['<h1>'],
      ],
      [
        'markdown' => '## An Unexpected Party',
        'search_elements' => ['<h2>'],
      ],
      [
        'markdown' => "* Smaug\n* Glaurang\n* Ancalagon",
        'search_elements' => ['<ul>', '<li>'],
      ],
      [
        'markdown' => "1. Thorin\n1. Fili\n1. Kili",
        'search_elements' => ['<ol>', '<li>'],
      ],
      [
        'markdown' => "**Gothmog**",
        'search_elements' => ['<strong>'],
      ],
      [
        'markdown' => "*Glorfindel*",
        'search_elements' => ['<em>'],
      ],
    ];
    foreach ($test_data as $test_datum) {
      $html = $process_plugin->transform(
        $test_datum['markdown'],
        $this->migrateExecutable,
        $this->row,
        '',
      );
      foreach ($test_datum['search_elements'] as $element) {
        self::assertStringContainsString($element, $html);
      }
    }
  }

  /**
   * Utility function to help test markdown that requires extensions.
   *
   * @param string $extension
   *   The extension we want to test, as it would be listed in the plugin
   *   configuration.
   * @param string $markdown
   *   The markdown we are to test the transformation of.
   * @param string[] $search_texts
   *   The text elements we are looking for in the output to deteremine success.
   */
  protected function testElementsRequiringExtensions(string $extension, string $markdown, array $search_texts): void {
    $process_plugin_with_no_extensions = new MarkdownToHtml([], '', []);
    $process_plugin_with_extension = new MarkdownToHtml(['markdown_extensions' => [$extension]], '', [],);
    $html = $process_plugin_with_no_extensions->transform($markdown, $this->migrateExecutable, $this->row, '',);
    foreach ($search_texts as $search_text) {
      self::assertStringNotContainsString($search_text, $html);
    }
    $html = $process_plugin_with_extension->transform($markdown, $this->migrateExecutable, $this->row, '',);
    foreach ($search_texts as $search_text) {
      self::assertStringContainsString($search_text, $html);
    }
  }

  /**
   * Test table extension.
   *
   * @covers ::transform
   */
  public function testTableExtension(): void {
    $this->testElementsRequiringExtensions('table', "|Race|Rings|\n|-----|-----|\n|Elves|3|\n|Dwarves| 7|\n|Men|9|\n|Maiar| 1|", ['<table>']);
  }

  /**
   * Test strikethrough extension.
   *
   * @covers ::transform
   */
  public function testStrikethroughExtension(): void {
    $this->testElementsRequiringExtensions('strikethrough', "~~Boromir~~", ['<del>']);
  }

  /**
   * Test attributes extension.
   *
   * @covers ::transform
   */
  public function testAttributesExtension(): void {
    $this->testElementsRequiringExtensions('attributes', "> What About Side By Side With A Friend?\n{: title='Legolas Speech'}", ['<blockquote title="Legolas Speech"']);
  }

  /**
   * Test autolink extension.
   *
   * @covers ::transform
   */
  public function testAutolinkExtension(): void {
    $this->testElementsRequiringExtensions('autolink', 'https://example.com', ['<a ']);
    $this->testElementsRequiringExtensions('autolink', 'eru@example.com', ['<a ']);
  }

  /**
   * Test description_list extension.
   *
   * @covers ::transform
   */
  public function testDescriptionListExtension(): void {
    $this->testElementsRequiringExtensions('description_list', "Palantír\n: A spherical stone used for synchronous communication at a distance\n: A tech company who we shall not comment upon further", [
      '<dd>',
      '<dl>',
      '<dt>',
    ]);
  }

  /**
   * Test github_flavored_markdown extension.
   *
   * @covers ::transform
   */
  public function testGithubFlavoredMarkdownExtension(): void {
    $this->testElementsRequiringExtensions('github_flavored_markdown', "https://example.com", ['<a ']);
    $this->testElementsRequiringExtensions('github_flavored_markdown', "~~Boromir~~", ['<del>']);
    $this->testElementsRequiringExtensions('github_flavored_markdown', "|Race|Rings|\n|-----|-----|\n|Elves|3|\n|Dwarves| 7|\n|Men|9|\n|Maiar| 1|", ['<table>']);
    $this->testElementsRequiringExtensions('github_flavored_markdown', " - [X] Retrieve Silmaril from Iron Crown\n - [ ] Return Silmaril to Thingol\n - [ ] Marry Luthien", [
      '<input disabled="" type="checkbox">',
      '<input checked="" disabled="" type="checkbox">',
    ]);
  }

  /**
   * Test task_list extension.
   *
   * @covers ::transform
   */
  public function testTaskListExtension(): void {
    $this->testElementsRequiringExtensions('task_list', " - [X] Retrieve Silmaril from Iron Crown\n - [ ] Return Silmaril to Thingol\n - [ ] Marry Luthien", [
      '<input disabled="" type="checkbox">',
      '<input checked="" disabled="" type="checkbox">',
    ]);
  }

}
